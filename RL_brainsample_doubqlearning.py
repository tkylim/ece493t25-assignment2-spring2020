import numpy as np
import pandas as pd

from utils import epsilon_greedy
from utils import check_state_exist

class rlalgorithm:

    def __init__(self, actions, learning_rate=0.1, reward_decay=0.9, e_greedy=0.1):
        self.actions = actions  
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table1 = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.q_table2 = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name="Double Q Learning ({})".format(learning_rate)

    def choose_action(self, observation):
        # Should add this to both tables
        self.q_table1 = check_state_exist(self.q_table1, observation, self.actions)
        self.q_table2 = check_state_exist(self.q_table2, observation, self.actions)

        return self.double_epsilon_greedy(observation, self.epsilon)

    def learn(self, s, a, r, s_):
        # Should add this to both tables
        self.q_table1 = check_state_exist(self.q_table1, s_, self.actions)
        self.q_table2 = check_state_exist(self.q_table2, s_, self.actions)

        if s_ != 'terminal':
            a = self.choose_action(str(s))

            greedy_action1 = epsilon_greedy(self.q_table1, s_, 0, self.actions)
            greedy_action2 = epsilon_greedy(self.q_table2, s_, 0, self.actions)
            
            maxq1 = self.q_table2.loc[s, greedy_action1]
            maxq2 = self.q_table1.loc[s, greedy_action2]

            # 50% chance of training the other function
            if np.random.uniform() > 0.5: 
                self.q_table1.loc[s, a] = self.q_table1.loc[s, a] + self.lr * (r + self.gamma * maxq1 - self.q_table1.loc[s,a])
            else:
                self.q_table2.loc[s, a] = self.q_table2.loc[s, a] + self.lr * (r + self.gamma * maxq2 - self.q_table2.loc[s,a])
        else:
            if np.random.uniform() > 0.5: 
                self.q_table1.loc[s,a] = r  # next state is terminal
            else:
                self.q_table2.loc[s,a] = r  # next state is terminal

        return s_, a

# Code modified from RL_brainsample_hacky_PI.py
    def double_epsilon_greedy(self, s, epsilon):
        if np.random.uniform() >= epsilon:
            maxq = float("-inf")
            greedy_actions = []

            for a in range(len(self.actions)):
                q = self.q_table1.loc[s, a] + self.q_table2.loc[s, a]
                if q < maxq:
                    continue

                if q > maxq:
                    maxq = q
                    greedy_actions.clear()

                greedy_actions.append(a)

            action = np.random.choice(greedy_actions)
        else:
            action = np.random.choice(self.actions)

        return action
