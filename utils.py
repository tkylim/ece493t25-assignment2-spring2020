import numpy as np
import pandas as pd

# Code modified from RL_brainsample_hacky_PI.py
def epsilon_greedy(q_table, s, epsilon, actions):
    if np.random.uniform() >= epsilon:
        state_action = q_table.loc[s, :]
        action = np.random.choice(state_action[state_action == np.max(state_action)].index)
    else:
        action = np.random.choice(actions)

    return action

# Code modified from RL_brainsample_hacky_PI.py
'''States are dynamically added to the Q(S,A) table as they are encountered'''
def check_state_exist(q_table, state, actions):
    if state not in q_table.index:
        # append new state to q table
        q_table = q_table.append(
            pd.Series(
                [0]*len(actions),
                index=q_table.columns,
                name=state,
            )
        )
    return q_table
