import numpy as np
import pandas as pd

from utils import check_state_exist
from utils import epsilon_greedy

class rlalgorithm:
    def __init__(self, actions, learning_rate=0.1, reward_decay=0.9, e_greedy=0.1):
        self.actions = actions
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name="Q Learning({})".format(learning_rate)

    def choose_action(self, observation):
        self.q_table = check_state_exist(self.q_table, observation, self.actions)

        return epsilon_greedy(self.q_table, observation, self.epsilon, self.actions)

    # Code modified from RL_brainsample_hacky_PI.py
    def learn(self, s, a, r, s_):
        self.q_table = check_state_exist(self.q_table, s_, self.actions)

        if s_ != 'terminal':
            a = self.choose_action(str(s))

            greedy_action = epsilon_greedy(self.q_table, s_, 0, self.actions)
            maxq = self.q_table.loc[s, greedy_action]

            self.q_table.loc[s, a] = self.q_table.loc[s, a] + self.lr * (r + self.gamma * maxq - self.q_table.loc[s,a])
        else:
            self.q_table.loc[s,a] = r  # next state is terminal

        return s_, a

