import numpy as np
import pandas as pd

from utils import epsilon_greedy
from utils import check_state_exist


class rlalgorithm:
    def __init__(self, actions, learning_rate=0.1, reward_decay=0.9, e_greedy=0.1):
        self.actions = actions
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name="Expected SARSA ({})".format(learning_rate)

    def choose_action(self, observation):
        self.q_table = check_state_exist(self.q_table, observation, self.actions)
        return epsilon_greedy(self.q_table, observation, self.epsilon, self.actions)

# Code modified from RL_brainsample_hacky_PI.py
    def learn(self, s, a, r, s_):
        self.q_table = check_state_exist(self.q_table, s_, self.actions)
        if s_ != 'terminal':
            a_ = self.choose_action(str(s_)) # choose with epsilon greedy
            expvalue = self.calc_expected_value(s_)
            self.q_table.loc[s, a] = self.q_table.loc[s, a] + self.lr *(r + self.gamma * expvalue - self.q_table.loc[s, a])
        else:
            self.q_table.loc[s, a] = r  # next state is terminal
        return s_, a_

    def calc_expected_value(self, s):
        greedy_action = epsilon_greedy(self.q_table, s, 0, self.actions)
        greedy_value = self.q_table.loc[s, greedy_action]
        exp_tot = 0

        state_action = self.q_table.loc[s, :]
        ngreedy = len(state_action[state_action == np.max(state_action)])
        num_actions = len(state_action)

        for a in range(num_actions):
            v = self.q_table.loc[s, a]
            if v == greedy_value:
                exp_tot += (1.0 - self.epsilon) / ngreedy * greedy_value
            else:
                exp_tot += self.epsilon / (num_actions - ngreedy) * v

        return exp_tot 
